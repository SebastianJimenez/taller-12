#Taller 12: Crear una función llamada simular_semiparabolico()
#que calcule las posiciones x,y de un objeto y retorne una lista
#con las tuplas calculadas para intervalos de tiempo de 0.2 segundos

def simular_semiparabolico(Vo, Yo):
    lista_resultados=[]
    
    g=-9.8
    t=0
    y=0

    while y>=0:
        x=Vo*t
        y=Yo+(1/2)*(g*t**2)
        t=t+0.2
        lista_resultados.append((x,y))
    return lista_resultados
r=simular_semiparabolico(1,50)

print(r)